import Foundation

class ImageClass: Codable {
    var uuid: String?
    var photoPath: String?
    var like: Bool?
    var comment: String?
    
    init() {
        self.uuid = UUID().uuidString
    }
    
    private enum CodingKeys: String, CodingKey {
        case uuid
        case photoPath
        case like
        case comment
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        uuid = try container.decodeIfPresent(String.self, forKey: .uuid)
        photoPath = try container.decodeIfPresent(String.self, forKey: .photoPath)
        like = try container.decodeIfPresent(Bool.self, forKey: .like)
        comment = try container.decodeIfPresent(String.self, forKey: .comment)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.uuid, forKey: .uuid)
        try container.encode(self.photoPath, forKey: .photoPath)
        try container.encode(self.like, forKey: .like)
        try container.encode(self.comment, forKey: .comment)
    }
    
}
