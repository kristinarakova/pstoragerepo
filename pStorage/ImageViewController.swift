import UIKit

class ImageViewController: UIViewController {
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var photoContainerView: UIView!
    @IBOutlet weak var commentsContainerView: UIView!
    @IBOutlet weak var topBarContainerView: UIView!
    
    @IBOutlet weak var backToStorageButton: UIButton!
    @IBOutlet weak var commentBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var likeBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var trashBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var underPhotoImageView: UIImageView!
    
    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    var imagesArray = [ImageClass]()
    let duration = 0.5
    var index: Int?
    
    override func viewDidLoad() {
        super .viewDidLoad()
        
        self.loadArray()
        
        self.hideKeyboard()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeDetected(_:)))
        leftSwipe.direction = .left
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeDetected(_:)))
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
        
        self.commentLabel.isUserInteractionEnabled = true
        
        let deleteTap = UILongPressGestureRecognizer(target: self, action: #selector(deleteComment(_:)))
        self.commentLabel.addGestureRecognizer(deleteTap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.gradientView.addGradientThreeColors(firstColor: .yellow, secondColor: .red, thirdColor: .systemIndigo, opacity: 1, startPoint: CGPoint(x: 0.0, y: 1.0), endPoint: CGPoint(x: 1.0, y: 0.0), radius: 0)
        
        self.displayLikeComment()
        
        self.commentLabel.layer.masksToBounds = true
        self.commentLabel.layer.cornerRadius = self.commentLabel.frame.height / 2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.commentLabel.layer.masksToBounds = true
        self.commentLabel.layer.cornerRadius = self.commentLabel.frame.height / 2
    }
    
    private func loadArray () {
        self.imagesArray = StorageManager.shared.load()
    }
    
    @IBAction func backToStorageButton(_ sender: UIButton) {
        
        guard let storage = storyboard?.instantiateViewController(withIdentifier: "StorageViewController") as? StorageViewController else {return}
        self.navigationController?.pushViewController(storage, animated: true)
    }
    
    @IBAction func commentBarButtonItem(_ sender: UIBarButtonItem) {
        self.addCommentAlert()
    }
    
    @IBAction func deleteComment(_ recognizer: UILongPressGestureRecognizer) {
        
        let alert = UIAlertController(title: "Do you want delete comment?", message: "The action is irreversible", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            if let index = self.index {
                let imageObject = self.imagesArray[index]
                
                imageObject.comment = ""
                
                StorageManager.shared.saveLikeComment(object: imageObject, index: index)
                self.loadArray()
            }
            self.commentLabel.isHidden = true
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func likeBarButtonItem(_ sender: UIBarButtonItem) {
        
        if let index = self.index {
            let imageObject = self.imagesArray[index]
            if let like = imageObject.like {
                if like == false {
                    imageObject.like = true
                    self.likeBarButtonItem.image = UIImage(systemName: "heart.fill")
                } else if like == true {
                    imageObject.like = false
                    self.likeBarButtonItem.image = UIImage(systemName: "heart")
                }
            }
            
            StorageManager.shared.saveLikeComment(object: imageObject, index: index)
            self.loadArray()
        }
    }
    
    @IBAction func trashBarButtonItem(_ sender: UIBarButtonItem) {
        
        self.alertTwoButtons(title: "Do you want delete photo?", message: "Attention: the action is irreversible", preferredStyle: .actionSheet, titleFirstButton: "Delete photo", styleFirstButton: .destructive, handlerFirst: { (_) in
            
            StorageManager.shared.saveArrayAfterDeleting(numberOfItem: self.index!)
            
            self.index! += 1
            if self.index! > self.imagesArray.count - 1 {
                self.index = 0
            }
            
            self.loadArray()
            
            self.photoImageView.image = StorageManager.shared.loadImage(fileName: self.imagesArray[self.index!].photoPath!)
        }, titleSecondButton: "Cancel", styleSecondButton: .cancel, handlerSecond: nil)
    }
    
    @IBAction func leftSwipeDetected (_ recognizer: UISwipeGestureRecognizer) {
        
        self.underPhotoImageView.frame.origin.x = self.photoContainerView.frame.width
        
        var nextPhoto = self.index! + 1
        if nextPhoto > self.imagesArray.count - 1 {
            nextPhoto = 0
        }
        self.underPhotoImageView.image = StorageManager.shared.loadImage(fileName: self.imagesArray[nextPhoto].photoPath!)
        
        UIView.animate(withDuration: self.duration, delay: 0, options: .curveLinear, animations: {
            
            self.underPhotoImageView.frame = self.underPhotoImageView.frame.offsetBy(dx: -1 * self.underPhotoImageView.frame.size.width, dy: 0.0)
            self.photoImageView.frame = self.photoImageView.frame.offsetBy(dx: -1 * self.photoImageView.frame.size.width, dy: 0.0)
        }, completion: { (_) in
            
            self.underPhotoImageView.frame.origin.x = self.photoContainerView.frame.width
            self.underPhotoImageView.image = nil
            self.photoImageView.frame.origin.x = self.photoContainerView.frame.origin.x
            
            self.index! += 1
            if self.index! > self.imagesArray.count - 1 {
                self.index = 0
            }
            
            self.photoImageView.image = StorageManager.shared.loadImage(fileName: self.imagesArray[self.index!].photoPath!)
            self.displayLikeComment()
        })
    }
    
    @IBAction func rightSwipeDetected (_ recognizer: UISwipeGestureRecognizer) {
        
        self.underPhotoImageView.frame.origin.x = -self.photoContainerView.frame.width
        
        var previousPhoto = self.index! - 1
        if previousPhoto < 0 {
            previousPhoto = self.imagesArray.count - 1
        }
        self.underPhotoImageView.image = StorageManager.shared.loadImage(fileName: self.imagesArray[previousPhoto].photoPath!)
        
        UIView.animate(withDuration: self.duration, delay: 0, options: .curveLinear, animations: {
            
            self.underPhotoImageView.frame = self.underPhotoImageView.frame.offsetBy(dx: 1 * self.underPhotoImageView.frame.size.width, dy: 0.0)
            self.photoImageView.frame = self.photoImageView.frame.offsetBy(dx: 1 * self.photoImageView.frame.size.width, dy: 0.0)
        }, completion: { (_) in
            
            self.underPhotoImageView.frame.origin.x = -self.photoContainerView.frame.width
            self.underPhotoImageView.image = nil
            self.photoImageView.frame.origin.x = self.photoContainerView.frame.origin.x
            
            self.index! -= 1
            if self.index! < 0 {
                self.index! = self.imagesArray.count - 1
            }
            
            self.photoImageView.image = StorageManager.shared.loadImage(fileName: self.imagesArray[self.index!].photoPath!)
            self.displayLikeComment()
        })
    }
    
    private func displayLikeComment () {

        self.photoImageView.image = StorageManager.shared.loadImage(fileName: imagesArray[self.index!].photoPath!)
            
            if imagesArray[self.index!].like == true {
                self.likeBarButtonItem.image = UIImage(systemName: "heart.fill")
            } else if imagesArray[self.index!].like == false {
                self.likeBarButtonItem.image = UIImage(systemName: "heart")
            }
            
            if imagesArray[self.index!].comment == "" {
                self.commentLabel.isHidden = true
            } else {
                self.commentLabel.isHidden = true
                
                self.commentLabel.isHidden = false
                self.commentLabel.text = self.imagesArray[self.index!].comment
                self.commentLabel.textColor = .black
                self.commentLabel.backgroundColor = .white
                self.commentLabel.layer.opacity = 0.8
                self.commentLabel.layer.masksToBounds = true
                self.commentLabel.layer.cornerRadius = self.commentLabel.frame.height / 2
            }
    }
    
    private func addCommentAlert () {
        
        let alert = UIAlertController(title: nil, message: "Comment", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Add a comment..."
        }
        
        let commentAction = UIAlertAction(title: "Add", style: .default) { (_) in
            
            let textField = alert.textFields![0] as UITextField
            
            if let index = self.index {
                let imageObject = self.imagesArray[index]
                if imageObject.comment == "" {
                    self.commentLabel.isHidden = false
                    self.commentLabel.text = textField.text
                    self.commentLabel.textColor = .black
                    self.commentLabel.backgroundColor = .white
                    self.commentLabel.layer.opacity = 0.8
                    self.commentLabel.layer.masksToBounds = true
                    self.commentLabel.layer.cornerRadius = self.commentLabel.frame.height / 2
                    
                    if let text = textField.text {
                        imageObject.comment = text
                    }
                    
                    StorageManager.shared.saveLikeComment(object: imageObject, index: index)
                    self.loadArray()
                } else {
                    self.commentAlreadyExist()
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (_) in
            // nothing
        }
        
        alert.addAction(commentAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}

