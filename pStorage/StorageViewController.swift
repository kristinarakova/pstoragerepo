import UIKit

class StorageViewController: UIViewController {
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var containerCollectionView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var addPhotoButton: UIButton!
    
    let imagePicker = UIImagePickerController()
    
    var imagesArray = [ImageClass]()
    var index: Int?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadArray()
        
        self.imagePicker.delegate = self
        
        self.gradientView.addGradientThreeColors(firstColor: .yellow, secondColor: .red, thirdColor: .systemIndigo, opacity: 1, startPoint: CGPoint(x: 0.0, y: 1.0), endPoint: CGPoint(x: 1.0, y: 0.0), radius: 0)
    }
    
    private func loadArray () {
        
        self.imagesArray = StorageManager.shared.load()
    }
    
    @IBAction func addPhotoButton(_ sender: UIButton) {
        
        self.imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
}

extension StorageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.originalImage] as? UIImage {
            
            let imagePath = StorageManager.shared.saveImage(image: pickedImage)
            
            let imageObject = ImageClass()
            imageObject.photoPath = imagePath
            imageObject.like = false
            imageObject.comment = ""
            
            StorageManager.shared.save(object: imageObject)
            
            self.loadArray()
            self.collectionView.reloadData()
            
            picker.dismiss(animated: true, completion: nil)
        }
    }
}

extension StorageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.imageView.image = StorageManager.shared.loadImage(fileName: imagesArray[indexPath.item].photoPath!)
        if imagesArray[indexPath.item].like == true {
         cell.likeImageView.image = UIImage(systemName: "heart.fill")
        } else if imagesArray[indexPath.item].like == false {
            cell.likeImageView.image = nil
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (self.collectionView.frame.size.width - 20 ) / 3
        return CGSize(width: side, height: side)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        self.index = indexPath.item
        
        let longTap = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        self.view.addGestureRecognizer(longTap)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func longTap (_ recognizer: UILongPressGestureRecognizer) {
        
        self.alertTwoButtons(title: "Do you want delete photo?", message: "Attention: the action is irreversible", preferredStyle: .actionSheet, titleFirstButton: "Delete photo", styleFirstButton: .destructive, handlerFirst: { (_) in
            
            if let index = self.index {
                StorageManager.shared.saveArrayAfterDeleting(numberOfItem: index)
            }
            
            self.loadArray()
            self.collectionView.reloadData()
        }, titleSecondButton: "Cancel", styleSecondButton: .cancel, handlerSecond: nil)
    }
    
    @objc func tap (_ recognizer: UILongPressGestureRecognizer) {
        
        guard let image = self.storyboard?.instantiateViewController(withIdentifier: "ImageViewController") as? ImageViewController else {return}
        image.index = self.index
        self.navigationController?.pushViewController(image, animated: true)
    }
    
}
