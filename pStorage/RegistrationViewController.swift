import UIKit
import SwiftyKeychainKit

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var pStorageLabel: UILabel!
    @IBOutlet weak var registrationLabel: UILabel!
    
    @IBOutlet weak var addUserButton: UIButton!
    @IBOutlet weak var backToEntranceButton: UIButton!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var arrayOfSequrityQuestions = ["What is your dream job?",
                                    "In what city did your parents meet?",
                                    "What was the name of your first pet?",
                                    "What is your favourite dish?",
                                    "What is your favourite cinema?"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.creatingUI(color: .white)
        
        self.hideKeyboard()
        
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.gradientView.addGradientThreeColors(firstColor: .yellow, secondColor: .red, thirdColor: .systemIndigo, opacity: 1, startPoint: CGPoint(x: 0.0, y: 1.0), endPoint: CGPoint(x: 1.0, y: 0.0), radius: 0)
    }
    
    @IBAction func addUserButton(_ sender: UIButton) {
        
        if self.usernameTextField.text != "" && self.passwordTextField.text != "" {
            self.userRegistration()
        } else {
            self.errorAlert()
        }
    }
    
    @IBAction func backToEntranceButton(_ sender: UIButton) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func creatingUI(color: UIColor) {
        
        self.pStorageLabel.textColor = color
        self.registrationLabel.textColor = color
        self.addUserButton.setTitleColor(color, for: .normal)
        self.backToEntranceButton.setTitleColor(color, for: .normal)
    }
    
    private func userRegistration () {
        
        let alert = UIAlertController(title: nil, message: "Answer security questions", preferredStyle: .alert)
        
        let label = UILabel(frame: CGRect(x: 0, y: 40, width: 270, height:18))
        label.text = "Please, enter a data"
        label.textAlignment = .center
        label.textColor = .red
        label.font = label.font.withSize(12)
        alert.view.addSubview(label)
        label.isHidden = true
        
        alert.addTextField { (textFieldOne) in
            let number = Int.random(in: 0...self.arrayOfSequrityQuestions.count - 1)
            textFieldOne.placeholder = self.arrayOfSequrityQuestions[number]
            try? StorageManager.shared.keychain.set(self.arrayOfSequrityQuestions[number], for: KeychainKeys.firstSecurityQuestion)
            self.arrayOfSequrityQuestions.remove(at: number)
        }
        alert.addTextField { (textFieldTwo) in
            let number = Int.random(in: 0...self.arrayOfSequrityQuestions.count - 1)
            textFieldTwo.placeholder = self.arrayOfSequrityQuestions[number]
            try? StorageManager.shared.keychain.set(self.arrayOfSequrityQuestions[number], for: KeychainKeys.secondSecurityQuestion)
            self.arrayOfSequrityQuestions.remove(at: number)
        }
        alert.addTextField { (textFieldThree) in
            let number = Int.random(in: 0...self.arrayOfSequrityQuestions.count - 1)
            textFieldThree.placeholder = self.arrayOfSequrityQuestions[number]
            try? StorageManager.shared.keychain.set(self.arrayOfSequrityQuestions[number], for: KeychainKeys.thirdSecurityQuestion)
            self.arrayOfSequrityQuestions.remove(at: number)
        }
        
        let actionContinue = UIAlertAction(title: "Continue", style: .default, handler: { (action) -> Void in
            
            let textFieldOne = alert.textFields![0]
            let textFieldTwo = alert.textFields![1]
            let textFieldThree = alert.textFields![2]
            
            if let userInputOne = textFieldOne.text, let userInputTwo = textFieldTwo.text, let userInputThree = textFieldThree.text {
                if userInputOne.isEmpty == false , userInputTwo.isEmpty == false, userInputThree.isEmpty == false {
                    
                    try? StorageManager.shared.keychain.set(userInputOne, for: KeychainKeys.firstSecurityQuestionAnswer)
                    try? StorageManager.shared.keychain.set(userInputTwo, for: KeychainKeys.secondSecurityQuestionAnswer)
                    try? StorageManager.shared.keychain.set(userInputThree, for: KeychainKeys.thirdSecurityQuestionAnswer)
                    
                    guard let username = self.usernameTextField.text else {return}
                    try? StorageManager.shared.keychain.set(username, for: KeychainKeys.usernameAccessTokenKey)
                    
                    guard let password = self.passwordTextField.text else {return}
                    try? StorageManager.shared.keychain.set(password, for: KeychainKeys.passwordAccessTokenKey)
                    
                    self.registrationCompleted()
                } else {
                    label.isHidden = false
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        })
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) -> Void in
            self.errorRegistration()
        })
        
        alert.addAction(actionContinue)
        alert.addAction(actionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
}
