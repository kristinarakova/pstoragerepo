import UIKit
import SwiftyKeychainKit

class ViewController: UIViewController {
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var pStorageLabel: UILabel!
    @IBOutlet weak var entranceLabel: UILabel!
    
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.creatingUI(color: .white)
        
        self.hideKeyboard()
        
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.gradientView.addGradientThreeColors(firstColor: .yellow, secondColor: .red, thirdColor: .systemIndigo, opacity: 1, startPoint: CGPoint(x: 0.0, y: 1.0), endPoint: CGPoint(x: 1.0, y: 0.0), radius: 0)
    }
    
    @IBAction func logInButton(_ sender: UIButton) {
        
        let tryData = try? StorageManager.shared.keychain.get(KeychainKeys.usernameAccessTokenKey)
        
        if tryData == nil {
            self.errorAddUser()
        } else {
            guard let username = self.usernameTextField.text else {return}
            guard let password = self.passwordTextField.text else {return}
            
            guard let saveUsername = try? StorageManager.shared.keychain.get(KeychainKeys.usernameAccessTokenKey) else {return}
            guard let savePassword = try? StorageManager.shared.keychain.get(KeychainKeys.passwordAccessTokenKey) else {return}
            
            if username == saveUsername && password == savePassword {
                guard let storage = storyboard?.instantiateViewController(withIdentifier: "StorageViewController") as? StorageViewController else {return}
                self.navigationController?.pushViewController(storage, animated: true)
            } else if password != savePassword {
                self.errorPasswordIncorrect()
            }
        }
    }
    
    @IBAction func registrationButton(_ sender: UIButton) {
        
        let saveUsername = try? StorageManager.shared.keychain.get(KeychainKeys.usernameAccessTokenKey)
        
        if saveUsername == nil {
            guard let registration = storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController else {return}
            self.navigationController?.pushViewController(registration, animated: true)
        } else {
            self.errorUserAlreadyExistAlert()
        }
    }
    
    @IBAction func forgotPasswordButton(_ sender: UIButton) {
        
        let saveUsername = try? StorageManager.shared.keychain.get(KeychainKeys.usernameAccessTokenKey)
        
        if saveUsername == nil {
            self.errorAddUser()
        } else {
            self.resetPassword()
        }
    }
    
    private func creatingUI (color: UIColor) {
        
        self.pStorageLabel.textColor = color
        self.entranceLabel.textColor = color
        self.logInButton.setTitleColor(color, for: .normal)
        self.registrationButton.setTitleColor(color, for: .normal)
        self.forgotPasswordButton.setTitleColor(color, for: .normal)
    }
    
    private func resetPassword () {
        
        let alert = UIAlertController(title: "Password recovery", message: "Answer security questions", preferredStyle: .alert)
        
        let label = UILabel(frame: CGRect(x: 0, y: 60, width: 270, height:18))
        label.text = "Please, enter a data"
        label.textAlignment = .center
        label.textColor = .red
        label.font = label.font.withSize(12)
        alert.view.addSubview(label)
        label.isHidden = true
        
        alert.addTextField { (textFieldOne) in
            textFieldOne.placeholder = try? StorageManager.shared.keychain.get(KeychainKeys.firstSecurityQuestion)
        }
        alert.addTextField { (textFieldTwo) in
            textFieldTwo.placeholder = try? StorageManager.shared.keychain.get(KeychainKeys.secondSecurityQuestion)
        }
        alert.addTextField { (textFieldThree) in
            textFieldThree.placeholder = try? StorageManager.shared.keychain.get(KeychainKeys.thirdSecurityQuestion)
        }
        
        let actionContinue = UIAlertAction(title: "Continue", style: .default, handler: { (action) -> Void in
            
            let textFieldOne = alert.textFields![0]
            let textFieldTwo = alert.textFields![1]
            let textFieldThree = alert.textFields![2]
            
            if let userInputOne = textFieldOne.text, let userInputTwo = textFieldTwo.text, let userInputThree = textFieldThree.text {
                if userInputOne.isEmpty == false , userInputTwo.isEmpty == false, userInputThree.isEmpty == false {
                    label.isHidden = true
                    
                    guard let firstAnswer = try? StorageManager.shared.keychain.get(KeychainKeys.firstSecurityQuestionAnswer) else {return}
                    guard let secondAnswer = try? StorageManager.shared.keychain.get(KeychainKeys.secondSecurityQuestionAnswer) else {return}
                    guard let thirdAnswer = try? StorageManager.shared.keychain.get(KeychainKeys.thirdSecurityQuestionAnswer) else {return}
                    
                    if userInputOne == firstAnswer && userInputTwo == secondAnswer && userInputThree == thirdAnswer {
                        
                        let label = UILabel(frame: CGRect(x: 0, y: 40, width: 270, height:18))
                        label.text = "Please, enter a data"
                        label.textAlignment = .center
                        label.textColor = .red
                        label.font = label.font.withSize(12)
                        alert.view.addSubview(label)
                        label.isHidden = true
                        
                        let alertNewPassword = UIAlertController(title: "Enter a new password", message: nil, preferredStyle: .alert)
                        
                        alertNewPassword.addTextField { (newPasswordTextField) in
                            newPasswordTextField.placeholder = "Password"
                        }
                        
                        let actionContinue = UIAlertAction(title: "Change password", style: .default, handler: { (action) -> Void in
                            let newPasswordTextField = alertNewPassword.textFields![0]
                            
                            if let newPassword = newPasswordTextField.text {
                                if newPassword.isEmpty == false {
                                    try? StorageManager.shared.keychain.set(newPassword, for: KeychainKeys.passwordAccessTokenKey)
                                } else {
                                    label.isHidden = false
                                    self.present(alertNewPassword, animated: true, completion: nil)
                                }
                            }
                        })
                        alertNewPassword.addAction(actionContinue)
                        self.present(alertNewPassword, animated: true, completion: nil)
                    }
                } else {
                    label.isHidden = false
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })
        let actionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) -> Void in
            //nothing
        })
        
        alert.addAction(actionContinue)
        alert.addAction(actionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
