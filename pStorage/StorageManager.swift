import Foundation
import UIKit
import SwiftyKeychainKit

class StorageManager {
    
    enum Keys: String {
        case controlQuestions = "controlQuestions"
    }
    
    static let shared = StorageManager()
    private init () {}
    
    let keychain = Keychain(service: "com.swifty.keychain")
    
    func saveControlQuestions(first: String, second: String, third: String) {
        
        let controlQuestions = [first, second, third]
        UserDefaults.standard.set(controlQuestions, forKey: Keys.controlQuestions.rawValue)
    }
    
    func loadControlQuestion() -> [String]? {
        
        guard let controlQuestions = UserDefaults.standard.value(forKey: Keys.controlQuestions.rawValue) as? [String] else {return nil}
        return controlQuestions
    }
    
    func save(object: ImageClass) {
        
        var array = StorageManager.shared.load()
        array.append(object)
        UserDefaults.standard.set(encodable: array, forKey: "photo")
    }
    
    func load() -> [ImageClass] {
        
        let array = UserDefaults.standard.value([ImageClass].self, forKey: "photo")
        
        if let array = array {
            return array
        } else {
            return []
        }
    }
    
    func saveLikeComment(object: ImageClass, index: Int) {
        
        var array = StorageManager.shared.load()
        array.remove(at: index)
        array.insert(object, at: index)
        UserDefaults.standard.set(encodable: array, forKey: "photo")
    }
    
    func saveArrayAfterDeleting(numberOfItem: Int) {
        
        var array = StorageManager.shared.load()
        array.remove(at: numberOfItem)
        UserDefaults.standard.set(encodable: array, forKey: "photo")
    }
    
    func saveImage(image: UIImage) -> String? {
        
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        
        let fileName = UUID().uuidString
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return nil}
        
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
        }
        
        do {
            try data.write(to: fileURL)
            return fileName
        } catch let error {
            print("error saving file with error", error)
            return nil
        }
        
    }
    
    
    func loadImage (fileName: String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            
            return image
            
        }
        return nil
    }
}

extension KeychainKeys {
    
    static let usernameAccessTokenKey = KeychainKey<String>(key: "usernameAccessTokenKey")
    static let passwordAccessTokenKey = KeychainKey<String>(key: "passwordAccessTokenKey")
    
    static let firstSecurityQuestion = KeychainKey<String>(key: "firstSecurityQuestion")
    static let secondSecurityQuestion = KeychainKey<String>(key: "secondSecurityQuestion")
    static let thirdSecurityQuestion = KeychainKey<String>(key: "thirdSecurityQuestion")
    
    static let firstSecurityQuestionAnswer = KeychainKey<String>(key: "firstSecurityQuestionAnswer")
    static let secondSecurityQuestionAnswer = KeychainKey<String>(key: "secondSecurityQuestionAnswer")
    static let thirdSecurityQuestionAnswer = KeychainKey<String>(key: "thirdSecurityQuestionAnswer")
}
